<?php

namespace App\Repository;

use App\Entity\Post;

class PostRepository
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }
    public function findAll()
    {
        $query = $this->pdo->prepare('SELECT * FROM Post');
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $post) {

            $post = $this->sqlToPost($post);

            $list[] = $post;
        }

        return $list;
    }
    public function add(Post $post): void
    {

        $query = $this->pdo->prepare('INSERT INTO Post (title,author,content) VALUES (:title,:author,:content)');

        $query->bindValue(':title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue(':author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue(':content', $post->getContent(), \PDO::PARAM_STR);

        $query->execute();

        $post->setId(intval($this->pdo->lastInsertId()));
    }
    public function findById(int $id): ?Post
    {

        $query = $this->pdo->prepare('SELECT * FROM Post WHERE id=:idPlaceholder');

        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);

        $query->execute();

        $post = $query->fetch();

        if ($post) {

            return $this->sqlToPost($post);
        }

        return null;
    }
    private function sqlToPost(array $post): Post
    {
        return new Post($post['title'], $post['author'], new \DateTime($post['postDate']), $post['content'], $post['id']);
    }

    public function delete(int $id): void
    {
        $query = $this->pdo->prepare('DELETE FROM Post WHERE id = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }

    public function update($title, $author, $content, $id): void
    {

        $query = $this->pdo->prepare('UPDATE Post
        SET title = :title, author = :author, content = :content WHERE id = :id');

        $query->bindValue(':title', $title, \PDO::PARAM_STR);
        $query->bindValue(':author', $author, \PDO::PARAM_STR);
        $query->bindValue(':content', $content, \PDO::PARAM_STR);
        $query->bindValue(':id', $id, \PDO::PARAM_INT);

        $query->execute();
    }
}
