<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;

class PostController extends AbstractController
{

   /**
    * @Route("/", name="PostCtrl")
    */
   public function PostTwig(Request $request)
   {
      $repo = new PostRepository();
      $newPost = null;
      $author = $request->get("author");
      $title = $request->get("title");
      $content = $request->get("content");

      if ($author && $title && $content) {

         $newPost = new Post($title, $author, new \DateTime, $content);

         $repo->add($newPost);
         return $this->redirectToRoute('PostCtrl');
      }
      $postCtrl = $repo->findAll();
      return $this->render('Post.html.twig', ['postCtrl' => $postCtrl]);
   }

   /**
    * @Route ("/{id}", name="ReadPost")
    */
   public function readPost(int $id)
   {
      $repo = new PostRepository();
      $post = $repo->findById($id);

      return $this->render('ReadPost.html.twig', [
         'post' => $post
      ]);
   }

 

/**
    * @Route("/del/{id}", name="delete_post")
    */
    public function deletePost(int $id)
    {
       $repo = new PostRepository();
       $repo->delete($id);
       return $this->redirectToRoute('PostCtrl');
    }


/**
   * @Route("/post/update/{id}", name = "update_post")
   */
  public function update(int $id, Request $request)
  {
     $repo = new PostRepository();

     $author = $request->get("author");
     $title = $request->get("title");
     $content = $request->get("content");
     if ($author && $title && $content) {
        $repo->update($title,$author,$content,$id);
     }
     $postView = $repo->findById($id);

     return $this->render('ModifPost.html.twig', ['PostCtrl' => $postView]);
  }
 }




