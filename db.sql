-- TODO 4 : typer les champs de la table post
CREATE TABLE Post ( 
    id INT PRIMARY KEY NOT NULL auto_increment, 
    title VARCHAR(65), 
    author VARCHAR(65),
    postDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    content VARCHAR(250)
);

-- TODO 5 : insert into post (jeux de données)
INSERT INTO Post (title,author,content) VALUES ('Environnement', 'Jean-Paul', 'News à Paris');
INSERT INTO Post (title,author,content) VALUES ('Politique', 'Pierre-Henry', 'News à Marseille');


