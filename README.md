#Projet Blog

L'objectif est de créer un blog avec les fonctionnalités suivantes: 

Fonctionnalités attendues

Créer des articles
Consulter la liste des articles
Consulter un article spécifique
Modifier ou supprimer un article.

Tout sa en utilisant symphony et une base de données (mariaDB).

J'ai décidé de faire un blog d'information (sur les actualités ou tout le monde peut ajouter des informations).

Voici les maquettes du site:

![](Maquettes/Accueil.png)
![](Maquettes/Lecturedel'article.png)
![](Maquettes/Modifdel'article.png)
